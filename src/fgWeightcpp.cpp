#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericMatrix fgWeightcppRow(NumericMatrix X, Function weight_fun) {
  int n = X.nrow();
  int m = X.ncol();
  
  NumericVector mu_vect(m);
  
  mu_vect = rowMeans(X);
  
  NumericVector p_vect = mu_vect / 2;
  
  NumericVector weights = weight_fun(p_vect);
  
  NumericMatrix X2(n, m);
  
  for(int j = 0; j < m; j++) {
    for(int i = 0; i < n; i++) {
      X2(i, j) = (X(i, j) - mu_vect[i]) * weights[i];
    }
  }
  
  return(X2);
}

// [[Rcpp::export]]
NumericMatrix fgWeightcppCol(NumericMatrix X, Function weight_fun) {
  int m = X.ncol();
  int n = X.nrow();
  
  Rcpp::NumericVector mu_vect(m);
  
  mu_vect = colMeans(X);
  
  NumericVector p_vect = mu_vect / 2;
  
  NumericVector weights = weight_fun(p_vect);
  
  NumericMatrix X2(n, m);
  
  for(int j = 0; j < m; ++j) {
    NumericMatrix::Column curr_col = X( _, j);
    NumericMatrix::Column rcurr_col = X2( _, j);
    rcurr_col = (curr_col - mu_vect[j]) * weights[j];
  }
  
  return(X2);
}

// // [[Rcpp::export]]
// NumericMatrix fgWeightcppCol(NumericMatrix X, Function weight_fun) {
//   int n = X.nrow();
//   int m = X.ncol();
//   
//   NumericVector mu_vect(m);
//   
//   mu_vect = colMeans(X);
//   
//   NumericVector p_vect = mu_vect / 2;
//   
//   NumericVector weights = weight_fun(p_vect);
//   
//   NumericMatrix X2(n, m);
//   
//   for(int j = 0; j < m; j++) {
//     for(int i = 0; i < n; i++) {
//       X2(i, j) = (X(i, j) - mu_vect[j]) * weights[j];
//     }
//   }
//   
//   return(X2);
// }

