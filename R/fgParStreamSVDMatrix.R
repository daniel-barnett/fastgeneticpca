#' @describeIn fgParStreamSVD Parallel streaming SVD on \code{matrix} objects. Please note the warning below.
#' @export

fgParStreamSVD.matrix <- function(X, k, l, block.size, base.decomp.fun, merge.decomp.fun = mergeSVD, base.args = NULL, n.cores = 1) {
  require(parallel)

  total.snps <- ncol(X)

  n.blocks <- ceiling(total.snps / block.size)

  block.start <- (0:(n.blocks - 1)) * block.size + 1
  block.end <- c((1:(n.blocks - 1)) * block.size, total.snps)

  overall.decomp <- NULL

  cl.sock <- makeCluster(n.cores, type = "SOCK", timeout = 120)
  clusterExport(cl.sock, "base.decomp.fun", envir = environment())
  clusterExport(cl.sock, "base.args", envir = environment())

  input.blocks <- lapply(1:n.blocks, function(i) X[,block.start[i]:block.end[i]])

  tryCatch(
    for (i in seq.int(1, n.blocks, by = n.cores)) {

      if (n.blocks < i + n.cores) {
        Z.list <- parLapply(cl.sock, input.blocks[i:n.blocks], function(inputblock) do.call(base.decomp.fun, c(list(inputblock), base.args)))
      } else {
        Z.list <- parLapply(cl.sock, input.blocks[i:(i+n.cores-1)], function(inputblock) do.call(base.decomp.fun, c(list(inputblock), base.args)))
      }

      for (j in 1:length(Z.list)) {
        if(is.null(overall.decomp)) {
          overall.decomp <- Z.list[[j]]
        } else {
          overall.decomp <- merge.decomp.fun(overall.decomp, Z.list[[j]], k = k + l)
        }
      }

    }, finally = stopCluster(cl.sock))

  overall.decomp$u <- overall.decomp$u[, 1:k]
  overall.decomp$d <- overall.decomp$d[1:k]

  overall.decomp
}
