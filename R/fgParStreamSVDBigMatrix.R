#' @describeIn fgParStreamSVD Parallel streaming SVD on \code{big.matrix} objects.
#' @export

fgParStreamSVD.big.matrix <- function(X, k, l, block.size, 
                                      base.decomp.fun, merge.decomp.fun = mergeSVD, base.args = NULL, 
                                      n.cores = 1, cluster = NULL) {
  total.snps <- ncol(X)

  n.blocks <- ceiling(total.snps / block.size)

  block.start <- (0:(n.blocks - 1)) * block.size + 1
  block.end <- c((1:(n.blocks - 1)) * block.size, total.snps)
  
  overall.decomp <- NULL

  X.desc <- describe(X)

  if(is.null(cluster)) {
    cl.sock <- parallel::makeCluster(n.cores, type = "SOCK", timeout = 120)
  } else {
    cl.sock <- cluster
    n.cores <- length(cluster)
  }
  
  parallel::clusterEvalQ(cl.sock, { library(bigmemory); 1 })
  
  parallel::clusterExport(cl.sock, "base.decomp.fun", envir = environment())
  parallel::clusterExport(cl.sock, "base.args",       envir = environment())
  parallel::clusterExport(cl.sock, "block.start",     envir = environment())
  parallel::clusterExport(cl.sock, "block.end",       envir = environment())
  parallel::clusterExport(cl.sock, "X.desc", envir = environment())

  parallel::clusterEvalQ(cl.sock, { A <- attach.big.matrix(X.desc); 1 })

  tryCatch(
    for (i in seq.int(1, n.blocks, by = n.cores)) {
      if (n.blocks < i + n.cores) {
        Z.list <- parallel::parLapply(cl.sock, i:n.blocks, function(i) do.call(base.decomp.fun, c(list(A[,block.start[i]:block.end[i]]), base.args)))

      } else {
        Z.list <- parallel::parLapply(cl.sock, i:(i+n.cores-1), function(i) do.call(base.decomp.fun, c(list(A[,block.start[i]:block.end[i]]), base.args)))
      }

      for (j in 1:length(Z.list)) {
        if(is.null(overall.decomp)) {
          overall.decomp <- Z.list[[j]]
        } else {
          overall.decomp <- merge.decomp.fun(overall.decomp, Z.list[[j]], k = k + l)
        }
      }

    }, finally = { if(is.null(cluster)) parallel::stopCluster(cl.sock) })

  overall.decomp$u <- overall.decomp$u[, 1:k]
  overall.decomp$d <- overall.decomp$d[1:k]

  overall.decomp
}
