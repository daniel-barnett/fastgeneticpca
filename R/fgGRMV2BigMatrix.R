fgGRM_v2.big.matrix = function(X, weighting = "unitvariance", alpha = 1, beta = 25, 
                           stream = FALSE, block.size = NULL, 
                           subj.incl = 1:nrow(X), MAF.line = 0.01, 
                           return.rare = TRUE, return.common = TRUE, return.all = TRUE,
                           parallel = FALSE, n.cores = NULL,
                           divide.by.snp = FALSE, ...) {
  ## Parameters of the data
  
  total.subj <- length(subj.incl)
  total.snps <- ncol(X)
  
  n.snps.common <- 0
  n.snps.rare <- 0
  
  ## Not streaming is equivalent to having a block that's the whole thing
  
  if(!stream) {
    block.size <- total.snps
  }
  
  ## Error checking
  
  if(MAF.line < 0 || MAF.line > 0.5) {
    stop("MAF cutoff must be 0 < MAF <= 0.5")
  }
  
  if(total.subj > nrow(X)) {
    stop("More subjects selected than exist in data")
  }
  
  if(block.size > total.snps) {
    message("Block size larger than number of SNPs in data; using block size equal to number of SNPs")
    block.size <- total.snps
    stream <- FALSE
  }
  
  ## Determine which weighting strategy to use
  
  if(is.character(weighting)) {
    if(!(weighting %in% c("unitvariance", "unitvariance2", "beta", "uniform"))) {
      stop("Invalid weighting selection")
    }
    
    weight.fun <- switch(weighting,
                         unitvariance  = function(p.vect) 1 / sqrt(p.vect * (1 - p.vect)),
                         unitvariance2 = function(p.vect) 1 / sqrt(2 * p.vect * (1 - p.vect)),
                         beta          = function(p.vect) dbeta(p.vect, alpha, beta),
                         uniform       = function(p.vect) 1)
  } else if(is.function(weighting)) {
    weight.fun <- weighting
  } else {
    stop("Invalid weighting argument. Should be either a function or a character string")
  }
  
  ## Determine which GRMs are going to be constructed
  
  if(!any(return.all, return.common, return.rare)) {
    stop("Must return at least one GRM")
  }
  
  if(return.all) {
    GRM.all <- matrix(0, nrow = total.subj, ncol = total.subj)
  } else {
    GRM.all <- NULL
  }
  
  if(return.common) {
    GRM.common <- matrix(0, nrow = total.subj, ncol = total.subj)
  } else {
    GRM.common <- NULL
  }
  
  if(return.rare) {
    GRM.rare <- matrix(0, nrow = total.subj, ncol = total.subj)
  } else {
    GRM.rare <- NULL
  }
  
  ## How many blocks will be used?
  
  n.blocks <- ceiling(total.snps / block.size)
  message(n.blocks)
  
  block.start <- (0:(n.blocks - 1)) * block.size + 1
  block.end <- c((1:(n.blocks - 1)) * block.size, total.snps)
  
  X.desc <- describe(X)
  
  cl.sock <- parallel::makeCluster(n.cores, type = "SOCK", timeout = 120)
  parallel::clusterExport(cl.sock, "block.start", envir = environment())
  parallel::clusterExport(cl.sock, "block.end", envir = environment())
  parallel::clusterExport(cl.sock, "fgWeightR", envir = environment())
  parallel::clusterExport(cl.sock, "weighting", envir = environment())
  
  parallel::clusterEvalQ(cl.sock, library(bigmemory)) ##### Should this be requireNamespace()
  parallel::clusterExport(cl.sock, "X.desc", envir = environment())
  parallel::clusterEvalQ(cl.sock, {X <- attach.big.matrix(X.desc); 1})
  parallel::clusterEvalQ(cl.sock, fgWeight <- fgWeightR)
  
  # helperfun <- function(i, MAF.line, return.common, return.rare) {
  #   curr.block <- X[,block.start[i]:block.end[i]]
  #   maf <- colMeans(curr.block) / 2
  # 
  #   if(return.common) {
  #     which.common <- maf > MAF.line
  #     Z.common <- fgWeight(curr.block[, which.common, drop = FALSE], weighting = weighting, alpha = alpha, beta = beta, snp.dim = 2)
  #   } else {
  #     Z.common <- NULL
  #   }
  # 
  #   if(return.rare) {
  #     which.rare <- maf <= MAF.line & maf > 0
  #     Z.rare <- fgWeight(curr.block[, which.rare, drop = FALSE], weighting = weighting, alpha = alpha, beta = beta, snp.dim = 2)
  #   } else {
  #     Z.rare <- NULL
  #   }
  # 
  #   return(list(common = Z.common, rare = Z.rare))
  # }
  
  parallel::clusterExport(cl.sock, "helperfun", envir = .GlobalEnv)
  
  if(stream) {
    message("Genetic Relatedness Matrix progress:")
    pb <- txtProgressBar(min = 1, max = n.blocks, style = 3)
  }

  for (i in seq.int(1, n.blocks, by = n.cores)) {
    if(stream) setTxtProgressBar(pb, i)
    
    if (n.blocks < (i + n.cores)) {
      if(length(i:n.blocks) == 1) {
        curr.block <- X[,block.start[i]:block.end[i]]
          maf <- colMeans(curr.block) / 2

          if(return.common) {
            which.common <- maf > MAF.line
            Z.common <- fgWeight(curr.block[, which.common, drop = FALSE], weighting = weighting, alpha = alpha, beta = beta, snp.dim = 2)
          } else {
            Z.common <- NULL
          }

          if(return.rare) {
            which.rare <- maf <= MAF.line & maf > 0
            Z.rare <- fgWeight(curr.block[, which.rare, drop = FALSE], weighting = weighting, alpha = alpha, beta = beta, snp.dim = 2)
          } else {
            Z.rare <- NULL
          }

          Z.list <- list(list(common = Z.common, rare = Z.rare))
        
      } else {
        Z.list <- parallel::parLapplyLB(cl.sock, i:n.blocks, helperfun, MAF.line = MAF.line, return.common = return.common, return.rare = return.rare)
      }
    } else {
      Z.list <- parallel::parLapplyLB(cl.sock, i:(i+n.cores-1), helperfun, MAF.line = MAF.line, return.common = return.common, return.rare = return.rare)
    }
    
    parallel::clusterEvalQ(cl.sock, gc())
    
    for(Z in Z.list) {
      if(return.all | return.common) {
        xprod.common <- tcrossprod(Z[["common"]])
        if(return.common) GRM.common <- GRM.common + xprod.common
        if(return.all) GRM.all <- GRM.all + xprod.common
        rm("xprod.common")
      }
      
      if(return.all | return.rare) {
        xprod.rare <- tcrossprod(Z[["rare"]])
        if(return.rare) GRM.rare <- GRM.rare + xprod.rare
        if(return.all) GRM.all <- GRM.all + xprod.rare
        rm("xprod.rare")
      }

    }

  }
  if(stream) close(pb)
  
  parallel::stopCluster(cl.sock)
  
  return(list(all = GRM.all, common = GRM.common, rare = GRM.rare))
}
